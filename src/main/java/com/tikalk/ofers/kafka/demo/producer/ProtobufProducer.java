package com.edgehawk.kafka.protobuf.producer;

import io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializer;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.time.Instant;
import java.util.Properties;

import static com.tikalk.ofers.kafka.protobuf.MessageProtos.SimpleMessage;

public class ProtobufProducer {

    public static void main(String[] args) {
        ProtobufProducer protobufProducer = new ProtobufProducer();
        protobufProducer.writeMessage();
    }

    public void writeMessage() {
        Properties properties = getProtobufProperties();        

        try (Producer<String, SimpleMessage> producer = new KafkaProducer<>(properties);) {
            //prepare the message
            SimpleMessage simpleMessage =
            SimpleMessage.newBuilder()
                    .setData("Hello world - from protobuf")
                    .setDate(Instant.now().toString())
                    .build();

            System.out.println(simpleMessage);

            //prepare the kafka record
            ProducerRecord<String, SimpleMessage> record
                    = new ProducerRecord<>("protobuf-topic", null, simpleMessage);
            producer.send(record);
            producer.flush();
        } catch (Exception e) {
            System.err.println("failed: " + e.getMessage());
        }

    }

    private Properties getProtobufProperties() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaProtobufSerializer.class);
        properties.put(KafkaProtobufSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        return properties;
    }

}
